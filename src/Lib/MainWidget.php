<?php
namespace GCpluginGmaps\Lib;

use Cake\Controller\Controller;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Utility\Text;


class MainWidget
{

  public function widget($params)
  {
    $html = "<section>
      <h1>MAPA</h1>

      <div style=\"width:200px;height:200px\"id=\"map\"></div>
      <script>
        function initMap() {
          var uluru = {lat: -25.363, lng: 131.044};
          var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 4,
            center: uluru
          });
          var marker = new google.maps.Marker({
            position: uluru,
            map: map
          });
        }
      </script>

    <script async defer
    src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyBQrTAVnr_GnoNUvI0OujzG31waIz_Mzsg&callback=initMap\">
    </script>

    </section>";


    return $html;
  }

}
