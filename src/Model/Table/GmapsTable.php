<?php
namespace GCpluginGmaps\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use GCpluginGmaps\Lib\MainWidget;

class GmapsTable extends Table
{


    public function render($value)
    {
      $widget = new MainWidget();
      $result = $widget->widget($value);
      return $result;
    }


}
